import { AppRoutingModule } from "../app-routing.module";

export interface Guitar {
    id: number;
    model: string;
    manufacturer:string;   //alla variable data från api hemsidan
    bodyType: string;
    materials:GuitarMaterials;
    strings: number;
    image:string;

}

export interface GuitarMaterials{
neck: string;
fretboard: string;
body: string;


}