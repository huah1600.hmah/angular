import { useAnimation } from '@angular/animations';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable, of, switchMap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { User } from '../models/user.model';


const { apiUsers, apiKey } = environment; //url
@Injectable({
  providedIn: 'root'
})
export class LoginService {
  // module exposes the feature http client dependency injection,injected my http client into login service
  constructor(private readonly http: HttpClient) { }
  //---->>>>>>>>>>Models ,HttpClient ,Observables ,and Rx JS operators. Part 4
  public login(username: string): Observable<User> {
    return this.checkUsername(username)
      .pipe(
        switchMap((user: User | undefined) => {
          if (user === undefined) {//user does not exit
            return this.createUser(username);
          }
          return of(user);//RETURN A USER if IT EXIT
        })

      )

  }
  //login

  //check if user exits
  private checkUsername(username: string): Observable<User | undefined> {// if the user dosent exist
    return this.http.get<User[]>(`${apiUsers}?username=${username}`)//queary parameters // use this.http if you want to acces private injection
      .pipe(
        //Rxjs Operators
        map((response: User[]) => response.pop())
      )
  }


  //IF NOT user -Create a User
  private createUser(username: string): Observable<User> {

    const user = {
      username,
      favourites: []
    };
    const headers = new HttpHeaders({ // recives an object as an arguments
      "Content-Type": "application/json",
      "x-api-key": apiKey
    });

    return this.http.post<User>(apiUsers, user, {

      headers
    })


  }

}

//-- headers -> API kEY
// POST - Create items on the server
  //IF User || Created User ->Store user 
