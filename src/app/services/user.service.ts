import { Injectable } from '@angular/core';
import { StorageKeys } from '../enums/storage-keys.enum';
import { User } from '../models/user.model';
import { StorageUtil } from '../utils/storage.util';
/* this user service is going to keep track of the currently logged in user. */
/*responsibility of storing user should be the user service*/
@Injectable({
  providedIn: 'root'
})
export class UserService {
private _user?: User;
get user(): User | undefined{
  return this._user;

}

set user(user: User| undefined ){
  StorageUtil.storageSave<User>(StorageKeys.User,user!) //only execute this when user is defined 
  this._user =user;
}
  constructor() { 
    this._user =StorageUtil.storageRead<User>(StorageKeys.User);
  }

}
