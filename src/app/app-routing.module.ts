import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { GuitarCataloguePage } from "./pages/guitar-catalogue/guitar-catalogue.page";

import { LoginPage } from "./pages/login/login.page";
import { ProfilePage } from "./pages/profile/profile.page";

const routes: Routes =[

    {
        path:"",
        pathMatch: "full",         /**  pathmatch helps redirect loginpage to an empty path,now first page is not empty*/
        redirectTo: "/login"
    },
    {
        path: "login",
        component: LoginPage
    },
    {
        path: "guitars",
        component: GuitarCataloguePage     // linked all the components

    },
    {
        path: "profile",
        component: ProfilePage

    }
]

@NgModule({
imports:[
    RouterModule.forRoot(routes)
] , //Import a module
exports:[ 
    RouterModule]  // Expose module and it's features

})
export class  AppRoutingModule{


}