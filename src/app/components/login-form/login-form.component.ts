import { Component, EventEmitter, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user.model';
import { LoginService } from 'src/app/services/login.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})

export class LoginFormComponent {
  /*Well, we're going to create an event emitter, 
which will event emit events to the parent component 
that is hosting a login form. So the login form is currently 
embedded in the login page, which means the login page will receive 
that event notification. So in this instead of redirecting,
 we're going to say this dot login dot emit, right there. 
And if an error occurs, we need to handle that locally in the component
*/
  @Output() login: EventEmitter<void> = new EventEmitter();

  constructor(
    // not needed // private readonly router: Router, //So this router service allows us to do
    private readonly userService: UserService,
    private readonly loginService: LoginService) { }

  public loginSubmit(loginFrom: NgForm): void {
    //USERNAME!
    const { username } = loginFrom.value;
    //console.log(username);
    this.loginService.login(username)
      .subscribe({
        next: (user: User) => {
          // So we need to somehow redirect to this catalog page, and then display in that page, the guitars that we want to do..
          //this.router.navigateByUrl("/guiters")//this dot router, dot navigate by URL, and we want to go to the guitars page.
          this.userService.user =user; //set the user and save it to  local storage
          this.login.emit();
        },
        error: () => {
          //error occurs ,then handle that locally

        }


      })
  }

}
