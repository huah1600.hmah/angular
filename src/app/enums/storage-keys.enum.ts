/*  So let's rename this folder to enums. 
Use some of our cool TypeScript features. And enums are really easy.
 They're just values that you can access consistently without having to 
 retype string values or numbers. 
 It gives us like a nice way to write constant values. So export, enum. 
 storage keys, and let's say user is equal to maybe 
 what is this guitars user that's To tell us that and
  note that this is an equal sign, not a colon. 
Because this is not an object. 
It's an enum. So syntax is slightly different.  */

export enum StorageKeys {

User = "guitars-user"

}