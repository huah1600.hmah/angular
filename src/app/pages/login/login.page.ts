import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.css']
})
export class LoginPage implements OnInit {

  constructor(private readonly router: Router) { }

  ngOnInit(): void {
  }
  /*  And in that login event, we're redirecting to this page --> login.page.html*/
  handleLogin(): void {//And all this will do is redirect.  

    this.router.navigateByUrl("/guitars");
  }

}
